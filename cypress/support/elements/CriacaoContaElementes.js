
export default class CriacaoContaElementes {

    formBtnRadio                   ()  { return 'input[id="id_gender1"]'             }
    formCampoPrimeiroNome          ()  { return 'form input[id="customer_firstname"]'}
    formCampoUltimoNome            ()  { return '#customer_lastname'                 }
    formCampoSenha                 ()  { return 'label ~ input[type="password"]'     }
    formSeletorDataDia             ()  { return 'select[id="days"]'                  }
    formSeletorDataMes             ()  { return 'select[id="months"]'                }
    formSeletorDataAno             ()  { return 'select[id="years"]'                 }
    formInputPrimeiroNome          ()  { return 'label ~ input[id="firstname"]'      }
    formInputUltimoNome            ()  { return 'label ~ input[id="lastname"]'       }
    formSeletorEstado              ()  { return 'select[id="id_state"]'              }
    formSeletorCompania            ()  { return 'label ~ input[id="company"]'        }
    formInputEndRua                ()  { return 'label ~ input[id="address1"]'       }
    formInputCodigoPostal          ()  { return 'label ~ input[id="postcode"]'       }
    formInputCidade                ()  { return 'label ~ input[id="city"]'           }
    formSeletorPais                ()  { return 'select[id="id_country"]'            }
    formSeletorEstado              ()  { return 'select[id="id_state"]'              } 
    formCampoTextoArea             ()  { return 'label ~ textarea[id="other"]'       }
    formCampoTelefone              ()  { return 'label ~ input[id="phone"]'          }
    formCampoCelular               ()  { return 'label ~ input[id="phone_mobile"]'   }
    formCampoEndereçoAlternativo   ()  { return 'label ~ input[id="alias"]'          }
    BtnConfirmarFormulario         ()  { return 'button[id="submitAccount"]'         }
 }