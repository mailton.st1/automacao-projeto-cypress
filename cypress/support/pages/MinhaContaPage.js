/// <reference types="cypress"/>


import MinhaContaElements from '../elements/MinhaContaElements'
const minhaContaElements = new MinhaContaElements

import Utils from '../utils/utils'
const utils = new Utils;

//Variaveis
let resultsTextMinhaConta = 'My account'

export default class MinhaContaPage {
     verificarTextMinhConta(){
          utils.validarText(minhaContaElements.minhaConta(),resultsTextMinhaConta)
     }
}
