/// <reference types ="cypress" />

import HomeElements from '../elements/HomeElements'
const homeElements = new HomeElements

import Utils from '../utils/utils'
const utils = new Utils;


export default class HomePage{

    acessarSite(url){
        utils.navegar(url);
    }
    clicarBtnMinhaConta(){
        utils.click(homeElements.btnSignIn());
    }
}
