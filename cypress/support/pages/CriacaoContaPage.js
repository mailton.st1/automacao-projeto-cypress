/// <reference types ="cypress" />

import CriacaoContaElementes from '../elements/CriacaoContaElementes'
const criacaoContaElementes = new CriacaoContaElementes

import Utils from '../utils/utils'
const utils = new Utils

let Chance = require('chance');
let chance = new Chance();

//Variaveis
let primeiro_nome = chance.first();
let sobre_nome = chance.last();
let senha_padrao = '@Teste12345'
let company = chance.company();
let endereçoAlternativo = chance.address();
let cidade = 'Dallas';
let pais = 'Estados Unidos';
let estado = 'Texas';
let codigo_postal = '75206';
let mes = '6';
let dia = '3';
let ano = '1988';
let telefone = chance.phone({formatted: true});
let fone = chance.phone({formatted: true});
let endereco = '87895125, Testes';
let textoArea = 'Este formulario estar sendo executado por um testes automatizado';
let Mr = ' Mr.'

export default class CriacaoContaPage {

    clicarNoBtnRadioSr(){
        utils.clickForce(criacaoContaElementes.formBtnRadio())
    }
    esperarUmTempo(){
        utils.tempo(3000)
    } 
    PreencherCampoCustomizadoPrimeiroNome(){
        utils.preencherInputForce(criacaoContaElementes.formCampoPrimeiroNome(),primeiro_nome)
    }
    preencherCampoCustomizadoUltimoNome(){
        utils.preencherInputForce(criacaoContaElementes.formCampoUltimoNome(),sobre_nome);
    }
    preencherCampoSenha(){
        utils.preencherInputForce(criacaoContaElementes.formCampoSenha(),senha_padrao);
    }
    selecionarDataDia(){
        utils.select(criacaoContaElementes.formSeletorDataDia(), dia)
    }
    selecionarDataMes(){
        utils.select(criacaoContaElementes.formSeletorDataMes(), mes)
    }
    selecionarDataAno(){
        utils.select(criacaoContaElementes.formSeletorDataAno(), ano)
    }
    preencherCampoEstado() {
        utils.select(criacaoContaElementes.formSeletorEstado(), estado)
    }
    preencherCampoCompania() {
        utils.preencherInputForce(criacaoContaElementes.formSeletorCompania(), company)
    }
    preencherCampoRua() {
        utils.preencherInputForce(criacaoContaElementes.formInputEndRua(), endereco)
    }
    preencherCampoCodigoPostal() {
        utils.preencherInputForce(criacaoContaElementes.formInputCodigoPostal(), codigo_postal)
    }
    preencherCampoCidade() {
        utils.preencherInputForce(criacaoContaElementes.formInputCidade(), cidade)
    }
    preencherCampoPais() {
        utils.preencherInputForce(criacaoContaElementes.formSeletorPais(), pais)
    }
    preencherCampoTextoComentario() {
        utils.preencherInputForce(criacaoContaElementes.formCampoTextoArea(), textoArea)
    }
    preencherCampoTelefone() {
        utils.preencherInputForce(criacaoContaElementes.formCampoTelefone(), telefone)
    }
    preencherCampoPhone() {
        utils.preencherInputForce(criacaoContaElementes.formCampoCelular(), fone)
    }
    preencherCampoEndereço() {
        utils.preencherInputForce(criacaoContaElementes.formCampoEndereçoAlternativo(), endereçoAlternativo)
    }
    clicarBtnConfirmarFormulario() {
        utils.click(criacaoContaElementes.BtnConfirmarFormulario())
    }
}