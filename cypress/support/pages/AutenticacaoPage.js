/// <reference types ="cypress" />

import Utils from '../utils/utils'
const utils = new Utils

import AutenticacaoElements from '../elements/AutenticacaoElements'
const autenticacaoElements = new AutenticacaoElements

let Chance = require('chance');
let chance = new Chance();

export default class AutenticacaoPage {

    preencherComEmailCampoPrimeiroAcessoEmail(){
        utils.preencherInput(autenticacaoElements.campoPrimeiroAcessoEmail(),chance.email())
    }

    clicarBtnConfirmarPrimeiroAcesso(){
        utils.click(autenticacaoElements.btnConfirmarPrimeiroAcesso())
    }

}

