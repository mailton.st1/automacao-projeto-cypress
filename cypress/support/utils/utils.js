export default class Ultis {
     
     //Navegar na Url
     navegar(url){ 
          cy.visit(url)
     }
     //Funções de clicks
     click(elemet){ 
          cy.get(elemet).click() 
     }
     clickForce(elemet){ 
          cy.get(elemet).click({force:true}) 
     }
     //Funções preencher campos
     preencherInput(element, text){ 
          cy.get(element).type(text)
     }
     preencherInputForce(element, text){ 
          cy.get(element).type(text, {force:true})
     }
     //Função de tempo
     tempo(tempo){ 
          cy.wait(tempo) 
     }
     //Funções de select
     select(element, value){ 
          cy.get(element).select(value) 
     }
     //Funções de validação de texto
     validarText(element, text){ 
          cy.get(element).should('contain', text) 
     }
     
}