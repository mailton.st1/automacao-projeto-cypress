/// <reference types ="cypress" />

import CriacaoContaPage from '../pages/CriacaoContaPage'
const criacaoContaPage = new CriacaoContaPage

And("preencho o formulario de cadastro com dados validos e confirmar", () => {
    
    criacaoContaPage.esperarUmTempo();

    //Selecionar mês de nascimento
    criacaoContaPage.selecionarDataMes();
    //Selecionar ano de nascimento
    criacaoContaPage.selecionarDataAno();
        
    criacaoContaPage.PreencherCampoCustomizadoPrimeiroNome();
    //Preencher ultimo nome aleatorio
    criacaoContaPage.preencherCampoCustomizadoUltimoNome();
    //Escolher sexo masculino
    criacaoContaPage.clicarNoBtnRadioSr();
    //Preencher senha padrão de testes
    criacaoContaPage.preencherCampoSenha();
    //Selecionar dia de nascimento
    criacaoContaPage.selecionarDataDia();
    //Preencher a companhia
    criacaoContaPage.preencherCampoCompania();
    //Preencher o Endereço (Endereço, caixa postal, nome da empresa, etc.)
    criacaoContaPage.preencherCampoRua();
    //Preencher codigo postal
    criacaoContaPage.preencherCampoCodigoPostal();
    //Preencher cidade* (Obrigatorio)
    criacaoContaPage.preencherCampoCidade();
    //Preencher o estado (state)
    criacaoContaPage.preencherCampoEstado();
    //Preencher Informação adiciona
    criacaoContaPage.preencherCampoTextoComentario();
    //Preencher Telefone residencial
    criacaoContaPage.preencherCampoTelefone();
    //Preencher Informação adiciona
    criacaoContaPage.preencherCampoPhone();
    //Preencher  Pais* (Obrigatorio)
    criacaoContaPage.preencherCampoPais();
    //Preencher um endereço alternativo para referências futuras.*(Obrigatorio)
    criacaoContaPage.preencherCampoEndereço();
    //Selecionar o btm Registrar
    criacaoContaPage.clicarBtnConfirmarFormulario();
});

