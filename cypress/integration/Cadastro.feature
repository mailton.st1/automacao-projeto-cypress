
Feature: Cadastro

Scenario Outline: Cadastrar um usuário no sistema
    Given que acesso o sistema <sistema>
    And preencho com email valido no campo cadastro de usuario
    When preencho o formulario de cadastro com dados validos e confirmar
    Then devo verificar que estou logado com minha conta no sistema

    Examples:
        | sistema                        | 
        | http://automationpractice.com/ |  